import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:quiz_app/views/login_screen.dart';
import 'package:quiz_app/views/sign_up_screen.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.only(
            left: 20,
            right: 20,
            top: 20,
          ),
          child: Column(
            children: [
              const Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  "Welcome",
                  style: TextStyle(
                    shadows: [
                      Shadow(
                        color: Colors.black, // Choose the color of the shadow
                        blurRadius:
                            2.0, // Adjust the blur radius for the shadow effect
                        offset: Offset(2.0,
                            2.0), // Set the horizontal and vertical offset for the shadow
                      ),
                    ],
                    color: Color.fromARGB(255, 113, 47, 219),
                    fontSize: 30,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
              const Align(
                alignment: Alignment.centerLeft,
                child: SizedBox(
                  width: 250,
                  child: Text(
                    "Please login or sign up to continue using our app",
                    style: TextStyle(
                        color: Color.fromARGB(255, 0, 0, 0),
                        fontSize: 12,
                        fontWeight: FontWeight.w700),
                  ),
                ),
              ),
              Center(
                  child: Column(
                children: [
                  Lottie.asset("assets/images/animation_llqiwbs3.json",
                      animate: true, fit: BoxFit.fill),
                  const Text(
                    "Enter via Social Networks",
                    style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.w700,
                        color: Color.fromARGB(255, 113, 47, 219)),
                  ),
                ],
              )),
              const SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CircleAvatar(
                    radius: 35,
                    backgroundColor: const Color.fromARGB(255, 241, 240, 240),
                    child: SizedBox(
                      height: 30,
                      width: 30,
                      child: Image.asset(
                        "assets/images/facebook.png",
                        scale: 2,
                        filterQuality: FilterQuality.high,
                      ),
                    ),
                  ),
                  const SizedBox(
                    width: 18,
                  ),
                  CircleAvatar(
                    radius: 35,
                    backgroundColor: const Color.fromARGB(255, 241, 240, 240),
                    child: SizedBox(
                      height: 30,
                      width: 30,
                      child: Image.asset(
                        "assets/images/twitter.png",
                        scale: 2,
                        filterQuality: FilterQuality.high,
                      ),
                    ),
                  )
                ],
              ),
              const SizedBox(
                height: 20,
              ),
              const SizedBox(
                width: 90,
                child: Text(
                  "or login with email",
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w800,
                      color: Color.fromARGB(255, 113, 47, 219)),
                  softWrap: true,
                ),
              ),
              const SizedBox(
                height: 20,
              ),
              SizedBox(
                width: 350,
                height: 60,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (BuildContext context) {
                      return const SignUpScreen();
                    }));
                  },
                  style: ButtonStyle(
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      )),
                      backgroundColor: const MaterialStatePropertyAll(
                          Color.fromARGB(255, 113, 47, 219))),
                  child: const Text(
                    "Sign Up",
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w700,
                        color: Color.fromARGB(255, 255, 255, 255)),
                  ),
                ),
              ),
              const SizedBox(
                height: 30,
              ),
              Center(
                child: RichText(
                  text: TextSpan(
                    text: 'You already have an account?',
                    style: const TextStyle(
                        color: Colors.black,
                        fontSize: 13,
                        fontWeight: FontWeight.w700),
                    children: <WidgetSpan>[
                      WidgetSpan(
                        child: GestureDetector(
                          child: const Text(
                            ' Login',
                            style: TextStyle(
                                color: Color.fromARGB(255, 113, 47, 219),
                                fontSize: 13,
                                fontWeight: FontWeight.w700),
                          ),
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (BuildContext context) {
                              return const LoginScreen();
                            }));
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
